struct Car {
    wheels: Wheels,
    engine: Engine,
}

struct Wheels {
    size: u8,
}
struct Engine {
    power: u8,
}

impl Car {
    fn new(wheels: Wheels, engine: Engine) -> Car {
        Car { wheels, engine }
    }

    fn drive(&self) {
        println!(
            "Car with wheel size {} and engine power {} goes Wrooooom!",
            self.wheels.size, self.engine.power
        );
    }
}

fn main() {
    let big_wheels = Wheels { size: 20 };
    let small_wheels = Wheels { size: 10 };

    let big_engine = Engine { power: 255 };
    let small_engine = Engine { power: 50 };

    let small_car = Car::new(small_wheels, small_engine);
    let big_car = Car::new(big_wheels, big_engine);

    small_car.drive();
    big_car.drive();
}
